import numpy as np 
from math import sqrt 
import pandas as pd

def distance_euclidienne(coors_1,coors_2):
    x1,y1 = coors_1[0],coors_1[1]
    x2,y2 = coors_2[0],coors_2[1]
    dist = sqrt((x2-x1)**2+(y2-y1)**2)

    return dist

def energie_totale(trajet_coords): #[a,b,c,d] #[[a,b],[b,d],[d,c],[c,a]]
    energie = 0.0
    N = len(trajet_coords)
    for idx_ville in range(N-1):
        energie += distance_euclidienne(trajet_coords[idx_ville],trajet_coords[idx_ville+1])
    energie = distance_euclidienne(trajet_coords[N-1],trajet_coords[0])
    return energie

def perturbation(chemin):
    new_chemin = np.random.choice(range(len(chemin)), len(chemin), replace=False)
    return new_chemin

def initialiser(nbVilles):
    return list(np.random.permutation(nbVilles))

def read_data(file):
    df = pd.read_csv(file)
    array = df.to_numpy()
    return array


def algo_recuit_simule(T0, Tmin, delt):
    # f = energie

    while (T>Tmin or not critereConvergence(T)):
        while not equilibreThermodynamique:
            X_bis = voisin(X)
            delta_f = energie(X_bis) - energie(X)

            if delta_f<0 or acceptationMetropolis(delta_f,T):
                if delta_f < 0 and energie(X_bis)> f_min:
                        f_min = energie(X_bis)
                        X_min = X_bis

                energie(X) = energie(X_bis)
                X = X_bis

        T = refroidissement(T)

"""
def main():
    file = r"C:\Users\CYTech Student\OneDrive - CY Cergy Paris Université\Documents\ING2\IA\Recuit_sim\14.tsp"
    data = read_data(file)
    print(data)
main()