import tensorflow as tf
from keras.datasets import mnist
#Récupération et étude des données

data = mnist.load_data()



(x_train, y_train), (x_test, y_test) = mnist.load_data()

#Quel est le type et la taille de chacun des résultats ?
#appliquez min et max
#type des données
x_train.shape
y_train.shape
x_test.shape
y_test.shape

x_train.max()
x_train.min()

#Type des résultats : appliquez type(). On trouve numpy.ndarray

#En utilisant l'aide de numpy.array

x_train.dtype
x_train.shape
x_train.max()
x_train.min()

#On vérifie ce faisant que les x_ sont 
#des matrices 28*28 d'entiers compris entre 0 et 255 (niveaux de gris)
#On vérie ce faisant que les y_ sont des d'entiers compris entre 0 et 9 (des chiffres)
#On vérifie aussi que x_train et y_train contiennet 60000 éléments 
#              et que x_test et y_test contiennet 10000 éléments
			  
#Tranformations des images en un seule matrice de réels compris entre 0 et 1. L'objecif est de préparer le
#travail de notre réseau de neurones qui manipule des données réelles comprises entre 0 et 1
x_train = x_train.reshape(60000, 784)
x_test = x_test.reshape(10000, 784)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
#Visualisation de quelques images
import matplotlib as mpl
import matplotlib.pyplot as plt
A=x_train[0,:].reshape([28,28])
plt.figure()
plt.imshow(A, cmap='gray')
plt.grid(True)
plt.show()

#On peut aussi représenter plusieurs images avec le code suivant :

plt.figure(figsize=(5, 5), dpi=150)
for i in range(100):
  plt.subplot(10,20,i+1)
  plt.imshow(x_train[i,:].reshape([28,28]), cmap='gray')
  plt.axis('off')
plt.show()
