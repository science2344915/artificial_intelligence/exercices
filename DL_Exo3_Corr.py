# Création d'un RNN à une seule couche cachée/Sortie binaire
from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import pandas as pn
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

# Lecture du fichier de données
dataset = np.loadtxt("pima-indians-diabetes.csv", delimiter=",")
#dataset = pn.read_csv("pima-indians-diabetes.csv",header=None)




#X=myData.values[:,0:8]
#Y=myData.values[:,8]
# Séparer chaque ligne du fichier en un input et un output
X = dataset[:,0:8]
Y = dataset[:,8]
#Création des ensembles d'apprentissage et de test
X_train1 = X[:700,]
Y_train1 = Y[:700,]
X_test1 = X[700:,]
Y_test1 = Y[700:,]

#X_train1, X_test1, Y_train1, Y_test1 = train_test_split( X, Y, test_size = 0.3, random_state = 100)

# Création du modèle. Pour chaque niveau on précise
# le nombre de neurones et la fonction d'activation
model = Sequential()
model.add(Dense(12, input_dim=8, activation='sigmoid'))
model.add(Dense(5, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))
#Affichage d'un récapitulatif du modèle
model.summary()


# Compiler le modèle. Fonction d'erreur entropie, adam (descente du gradient), 
# sortie binaire ==> choix de la précision comme critère d'évaluation 
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
# Apprentissage
model.fit(X_train1, Y_train1, epochs=300, batch_size=10)
# Evaluation du modèle
scores = model.evaluate(X_test1, Y_test1)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))





Y_pred = model.predict(X_test1)
l_Y_pred_1 = [(lambda x: int(x>0.5))(x) for x in list(Y_pred)]
Y_pred_1 = np.array(l_Y_pred_1)
confusion = confusion_matrix(Y_pred_1, Y_test1)
