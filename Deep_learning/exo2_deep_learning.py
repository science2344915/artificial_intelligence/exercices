import tensorflow as tf
from keras.datasets import mnist

import matplotlib as mpl
import matplotlib.pyplot as plt
#Récupération et étude des données

data = mnist.load_data()

(x_train, y_train), (x_test, y_test) = mnist.load_data()

nb_x_train,x_train_d1,x_train_d2 = x_train.shape
nb_x_test,x_test_d1,x_test_d2 = x_test.shape

x_train = x_train.reshape(nb_x_train, x_train_d1*x_train_d2)
x_test = x_test.reshape(nb_x_test, x_test_d1*x_test_d2)
x_train = x_train.astype('float32')/255
x_test = x_test.astype('float32')/255

A=x_train[0,:].reshape([x_train_d1,x_train_d2])
plt.figure()
plt.imshow(A, cmap='gray')
plt.grid(True)
plt.show()

plt.figure(figsize=(5, 5), dpi=150)
for i in range(100):
  plt.subplot(10,20,i+1)
  plt.imshow(x_train[i,:].reshape([x_train_d1,x_train_d2]), cmap='gray')
  plt.axis('off')
plt.show()