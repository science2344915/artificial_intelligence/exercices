import numpy as np
from PIL import Image
from math import exp


n = 50
m = 10
p = 2


taille_image = (n,n)
taille_filtre = (m,m)
taille_conv = n-m+1
taille_pool = int((n-m+1)/p)

max_value_pixel = 255
biais = 1.0

image = (np.random.randint(max_value_pixel, size = taille_image))/max_value_pixel
filtre = (np.random.randint(max_value_pixel, size = taille_filtre))/max_value_pixel
conv = np.zeros((taille_conv,taille_conv))
pool = np.zeros((taille_pool,taille_pool))

def f_sigmoide(x):
    return 1/(1+exp(-x))

def pool_max(tab):
    return np.max(tab[i*p:i*p+p,j*p:j*p+p])

for i in range(taille_conv):
    for j in range(taille_conv):
        somme = 0
        for k in range(m):
            for l in range(m):
                somme+=filtre[k,l]*image[i+k,j+k]

        val = biais + somme 
        conv[i,j] = f_sigmoide(val)

for i in range(taille_pool):
    for j in range(taille_pool):
        pool[i,j] = pool_max(conv)

print(pool.shape)
print(pool)
img2 = Image.fromarray(conv, 'RGB')
img2.show()

img3 = Image.fromarray(pool, 'RGB')
img3.show()