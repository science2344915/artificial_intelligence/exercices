# Création d'un RNN à une seule couche cachée/Sortie binaire
from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import pandas as pn
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix


# Lecture du fichier de données
file_data = "./data/pima-indians-diabetes.csv"
dataset = np.loadtxt(file_data, delimiter=",")
dataset_shape = dataset.shape

rate_train_test = 3/4
index_limit = int(dataset_shape[0]*(rate_train_test))

x = dataset[:,0:8]
y = dataset[:,8]
#Création des ensembles d'apprentissage et de test
x_train = x[:index_limit,]
y_train = y[:index_limit,]
x_test = x[index_limit:,]
y_test = y[index_limit:,]

X_train, X_test, Y_train, Y_test = train_test_split(x, y, test_size = 0.3, random_state = 100)
