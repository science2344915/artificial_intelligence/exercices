import numpy as np

liste_etats = ("Cours1", "Cours2", "Cours3", "Pause", "Dormir", "Sortir", "Facebook")
liste_recompenses = (-2, -2, -2, 10, 1, 0, -1)

dict_recompenses = dict(map(lambda i,j : (i,j) , liste_etats,liste_recompenses))

matrice_transition = np.array([(0, 0.5, 0, 0, 0, 0, 0.5),
                               (0, 0, 0.8, 0, 0.2, 0, 0),
                               (0, 0, 0, 0.4, 0, 0.6, 0),
                               (0.2, 0.2, 0.4, 0, 0, 0, 0),
                               (0, 0, 0, 0, 0, 0, 0),
                               (0, 0, 0, 0, 1, 0, 0),
                               (0.1, 0, 0, 0, 0, 0, 0.9)])


gamma = 0.9
M = len(liste_etats)
I = np.identity(M)
P = matrice_transition
R_tmp = []


def calcul_valeur_Rs(etat,index):
    moyenne_Rs = 0
    ligne = matrice_transition[index,:]
    for index_bis,proba in enumerate(ligne):
        if proba != 0:
            moyenne_Rs += proba*liste_recompenses[index_bis]
    
    return moyenne_Rs


for index,etat in enumerate(liste_etats):
    R_tmp.append(calcul_valeur_Rs(etat,index))

R = np.array(R_tmp)

Vs = (R).dot(np.linalg.inv(I - gamma*P))  # calcul de v(s)

print(Vs)

