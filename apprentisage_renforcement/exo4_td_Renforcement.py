import numpy as np
import random

gamma = 0.9
e = 0.001
liste_etats = ("s1", "s2", "s3", "s4")
liste_action = ("bas", "haut", "gauche", "droite")
dictionnaire_regles = {"s1": {"bas": ("s3", 0), "haut": ("s1", -1),"gauche": ("s1", -1),"droite": ("s2", 0)},
                       "s2": {"bas": ("s4", -1/2), "haut": ("s2", -1),"gauche": ("s1", 0),"droite": ("s2", -1)},
                       "s3": {"bas": ("s3", -1), "haut": ("s1", 0),"gauche": ("s3", -1),"droite": ("s4", 0)},
                       "s4": {"bas": ("s1", 1), "haut": ("s1", 1),"gauche": ("s1", 1),"droite": ("s1", 1)} }

#Exemple de strategie pi
liste_strategie = {"s1": "bas", "s2":"haut", "s3":"haut", "s4":"gauche"}



def Q(etat, action,liste_vs):
    Rs = dictionnaire_regles[etat][action][1]
    HI = gamma*liste_vs[dictionnaire_regles[etat][action][0]]
    print("Rs + HI pour {} via {} avec {}: {} + {} = {}".format(dictionnaire_regles[etat][action][0],etat, action,Rs, HI,Rs+ HI))
    return Rs + HI


def vs(etat,liste_vs):
    liste_vs_tmp =[]
    for action in liste_action:
        value = Q(etat,action,liste_vs)
        liste_vs_tmp.append(value)

    print(liste_vs)
    #print("etat: {} | liste_vs_tmp: {} ".format(etat,liste_vs_tmp))
    vs_tmp_max = max(liste_vs_tmp)
    action_vs_tmp_max = liste_action[np.argmax(liste_vs_tmp)]
    return vs_tmp_max,action_vs_tmp_max

def value_iteration(liste_etats,liste_action, e):
    liste_vs = {"s1": 0, "s2":0, "s3":0, "s4":0}
    etat_max = max(liste_vs)
    v_max = liste_vs[etat_max]
    v_max_init = 1
    diff = abs(v_max_init - v_max)
    k = 0 
    while True:
        v_max_init = v_max
        print("---------------")
        print("iteration:", k+1)
        etat_max = max(liste_vs)
        v_max = liste_vs[etat_max]
        for etat in liste_etats:
            liste_vs[etat] = vs(etat,liste_vs)[0]
        
        print("liste_vs: ",liste_vs)
        etat_max = max(liste_vs)
        v_max = liste_vs[etat_max]
        diff = abs(v_max_init - v_max)
        print("strategie: ",etat_max)
        print("v_max: ",v_max)
        print("\n")
        k+=1
        
        if diff < e:
            break

def policy_evaluation(strategie,liste_etats,e,dico_vs):
    strategie_return = dict(strategie)
    dico_vs_tmp = dict(dico_vs)
    #while True:
        #delta = 0
    for etat in liste_etats:
        #v = dico_vs[etat]
        action = strategie[etat]
        #dico_vs_tmp[etat] = Q(etat,action,dico_vs_tmp)
        dico_vs_tmp[etat] = Q(etat,action,dico_vs)         ####QUESTION A POSER
            #delta = max(delta, abs(v - dico_vs[etat]))
            #print("delta",delta)

        #if delta < e:
            #break
    return dico_vs_tmp

def policy_improvement(strategie, liste_etats, liste_action,dico_vs):
    strategie_return = dict(strategie)
    for etat in liste_etats:
        liste_vs_tmp = []
        for action in liste_action:
            value = Q(etat,action,dico_vs)
            liste_vs_tmp.append(value)
        action_vs_tmp_max = liste_action[np.argmax(liste_vs_tmp)]
        #print("etat: {} | liste_vs_tmp: {} | indice: {} actions: {}\n".format(etat,liste_vs_tmp,np.argmax(liste_vs_tmp),liste_action))
        strategie_return[etat] = action_vs_tmp_max
    return strategie_return

def policy_iteration(liste_etats,liste_action, e):
    strategie_opt = {"s1": random.choice(liste_action),
    "s2": random.choice(liste_action),
    "s3": random.choice(liste_action),
    "s4": random.choice(liste_action)}

    dico_vs = dict(map(lambda i,j : (i,j) , liste_etats,np.zeros(len(liste_etats))))

    print("strategie_opt par hasard:")
    print(strategie_opt)
    
    k = 0
    
    while True: 
        print("---------------")
        print("iteration:", k+1)
        strategie = dict(strategie_opt)
        print("dico_vs avant eval: ", dico_vs)
        dico_vs = policy_evaluation(strategie,liste_etats,e,dico_vs)
        print("dico_vs apres eval: ", dico_vs)
        print("\n")
        
        strategie_opt = policy_improvement(strategie, liste_etats,liste_action,dico_vs)

        print("strategie en cours a iteration ", k+1)
        print("strategie",strategie)
        print("strategie_opt", strategie_opt, "\n")
  
        if strategie_opt == strategie:
            break
        
        k+=1

    print(strategie)
    


policy_iteration(liste_etats,liste_action,e)
#value_iteration(liste_etats,liste_action, e)