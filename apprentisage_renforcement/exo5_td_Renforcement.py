import numpy as np
import random
from tqdm import tqdm


shape = (3,3)
grid = np.zeros(shape)
grid[0,2]=1
grid[1,1]=-1
len_etats = shape[0]*shape[1]

#liste_action = ("bas", "haut", "gauche", "droite")
liste_action = ("haut", "droite", "bas", "gauche")
dictionnaire_action = {"bas": (1,0), "haut": (-1,0), "gauche": (0,-1), "droite": (0,1)}

gamma=0.7
alpha=0.2
learning_rate_limit = 0.8
nbIter = 200
etat_init = "2,0"
etat_end = "0,2"
etat_block = "1,1"

#Initialisation de la liste des etats
liste_etats = [0 for i in range(len_etats)]
for i in range(shape[0]):
    for j in range(shape[1]):
        liste_etats[j+i*shape[1]] = "{},{}".format(i,j)

print("INIT liste_etats:",liste_etats)


def rules(coords1,coords2): #Regles du dictionnaire des regles

    calcul_coords = list(map(lambda x,y: x+y, coords1,coords2))
    new_coords = list(map(lambda x: 0 if x<0 else 2 if x>2 else x, calcul_coords))
    return_coords = ",".join(list(map(str,new_coords)))

    return return_coords,0

def create_dico_rules(): #Construction du dictionnaire des regles
    dictionnaire_regles = {}

    for etat in liste_etats:
        dict_etat_action = {}
        for action in dictionnaire_action:
            dict_etat_action[action] = list(rules(dictionnaire_action[action],list(map(int, etat.split(",")))))
        dictionnaire_regles[etat] = dict_etat_action

    return dictionnaire_regles


def Q(etat,action,dico_regles,alpha):

    new_etat = dico_regles[etat][action][0]
    values_pos = list(map(int, new_etat.split(",")))

    Q_sa =dico_regles[etat][action][1]
    Rs = grid[values_pos[0],values_pos[1]]
    max_vs = max(list(map(lambda x: dico_regles[new_etat][x][1],liste_action)))
    HI = gamma*max_vs

    Q_upt = (1-alpha)*Q_sa + alpha*(Rs + HI)

    print("etat {} -> new_etat {}".format(etat,new_etat) )
    print("valeurs pour nouvel etat {} -> ?: {} / max_vs = {}".format(new_etat, list(map(lambda x: dico_regles[new_etat][x],liste_action)),max_vs))
    print("Changement: {} --> {} avec {}: {} = (1-{})*{} + {}*({} + {})".format(etat,new_etat, action,Q_upt, alpha,Q_sa,alpha,Rs,HI))

    return Q_upt


def random_actions():
    strategie_aleatoire = dict(map(lambda i,j : (i,j) , liste_etats,np.random.choice(liste_action,len(liste_etats), replace=True)))
    #print("strategie_aleatoire:",strategie_aleatoire)
    return strategie_aleatoire

def algo_Q_Learning(learning_rate_limit):
    
    dictionnaire_regles = create_dico_rules()
    print("dictionnaire_regles",dictionnaire_regles)

    #iterator = tqdm(range(nbIter+1)) #if debug else range(1,maxiter+1)
    for iteration in range(nbIter):
        etat = etat_init
        while etat_end!=etat and etat_block!=etat:

            var_random = random.uniform(0,1)
            #print("var_random",var_random)

            if var_random < learning_rate_limit: #On lance les actions aléatoirement
                strategie_aleatoire = random_actions()
                action = strategie_aleatoire[etat]
                print("\nACTION choisie pour etat: {} / au hasard: {} ".format(etat,action))

            elif var_random > learning_rate_limit:
                best = []
                liste_vs_tmp = list(map(lambda x: dictionnaire_regles[etat][x][1],liste_action))

                print("Actions possibles pour etat {}: {}".format(etat,list(map(lambda x: dictionnaire_regles[etat][x],liste_action))))
                print("liste_vs_tmp pour choix action",liste_vs_tmp)
                mx = max(liste_vs_tmp)

                for i in range(len(liste_vs_tmp)):
                    if (dictionnaire_regles[etat][liste_action[i]][1]==mx): 
                        best.append(i)
    
                choice = np.random.randint(len(best))
                action = liste_action[best[choice]]
                print("\nACTION choisie: {} / pour etat: {} / avec max: {}".format(action,etat,mx))


            return_Q = Q(etat,action,dictionnaire_regles,alpha)
            dictionnaire_regles[etat][action][1] = return_Q
        
            print("Valeur de la transition ({} -> {}) mise a jour avec: {}".format(etat,dictionnaire_regles[etat][action][0],return_Q))
            print("----------\n")
            etat = dictionnaire_regles[etat][action][0]


        print("Nouveau dictionnaire_regles:",dictionnaire_regles)
        print("----------\n")
    

    for etat_iterate in dictionnaire_regles:
    
        list_strategie_tmp = dict(map(lambda x : (x,dictionnaire_regles[etat_iterate][x][1])  , liste_action))
        index_optimal = max(list_strategie_tmp.items(), key=lambda k: k[1])

        print("Stratégie optimable pour {}: {} -> {}".format(etat_iterate,index_optimal[0],dictionnaire_regles[etat_iterate][index_optimal[0]][0] ))
        print(list(map(lambda x: dictionnaire_regles[etat_iterate][x],liste_action)))
        print("----------\n")
algo_Q_Learning(learning_rate_limit)