from nltk.tokenize import word_tokenize
import string
import nltk
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer


#Exo2 tp

text = 'In this tutorial, I\'m learning NLTK. It is an interesting platform.'

result = nltk.wordpunct_tokenize(text)
print(result)

stop_words = set(stopwords.words('english'))
pct =list(string.punctuation)
print(stop_words)
print(pct)

words = word_tokenize(text.lower())
print(words)


sentence_without_sw = []
sentence_sw = []
sentence_pct = []


for word in words:
    if word in stop_words:
        sentence_sw.append(word)
    if word in pct:
        sentence_pct.append(word)
    else:
        sentence_without_sw.append(word)



print(sentence_without_sw)

print(sentence_sw)

print(sentence_pct)

print(set(stopwords.words('french')))

stl = stopwords.fileids()
#print(set(stopwords.words('english')))
print(len(stl))
print(stl)


#Detecting the language of 2 documents
text_Fr="Au lendemain du feu vert européen, plusieurs Etats ont repris leur"
file = open('theguardian.txt','r')
read_file=file.read
text_Eng= str(nltk.Text(nltk.word_tokenize(read_file)))

#traitement des deux textes
languages_ratios=[]
tokens = word_tokenize(text_Fr)
words=[word.lower() for word in tokens]
for language in stopwords.fileids():
    stopwords_set=set(stopwords.words(language))
    words_set=set(words)
    common_elements=words_set.intersection(stopwords_set)
    print(language,common_elements)
    languages_ratios[language]=len(common_elements) 
    
#language "score"
most_rated_language = max (languages_ratios, key=languages_ratios.get)

def1 = 'A screenwriter is a person who writes screenplays'
tokens1 = word_tokenize(def1)
pos_tag1 = pos_tag(tokens1)


text = 'In this tutorial, I\'m learning NLTK. It is an interesting plateform.'
stop_words = set(stopwords.words('english'))
pct = list(string.punctuation)
print(stop_words)
words = word_tokenize(text.lower())
sentence_without_sw = []
sentence_sw = []
sentence_pct = []
for word in words :
    if word in stop_words :
        sentence_sw.append(word)
    elif word in pct :
        sentence_pct.append(word)
    else :
        sentence_without_sw.append(word)

print(sentence_without_sw)